.. o_ptb documentation master file, created by
   sphinx-quickstart on Tue Jul 23 11:49:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to o_ptb's documentation!
=================================

Introduction
------------

o_ptb is a class based library that runs on top of the well-known Psychtoolbox_. It also uses the VPixx_ system when connected. The latest version also include support for the LabJack U3 device for triggering.

The library tries to achieve two goals:

1. The same code should run and behave as equally as possible whether the Vpixx system is connected or not. If it is not connected, its capabilities will be emulated using the computer's hardware.
2. Make handling visual and auditory stimuli as well as triggers and responses easier and more coherent.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   install
   tutorial
   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. include:: links.rst