%% clear...
clear all global
close all
restoredefaultpath;

%% add path
o_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3');

%% configure...
ptb_cfg = o_ptb.PTB_Config;
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;

ptb = o_ptb.PTB.get_instance(ptb_cfg);

%% setup subsystems
ptb.setup_eyetracker();
ptb.setup_screen();

%% do eye position calibration
ptb.eyetracker_verify_eye_positions();

%% do calibration
ptb.eyetracker_calibrate()

%% start eyetracker
ptb.start_eyetracker();

%% do some triggering
ptb.setup_trigger();

triggers = 1:25;

for trg = triggers
  ptb.prepare_trigger(trg);
  ptb.schedule_trigger();
  
  ptb.play_without_flip();
  
  WaitSecs(0.2);
end %for

%% stop eyetracker
ptb.stop_eyetracker();

%% save data
ptb.save_eyetracker_data('snippets/eye_output/dig.mat');
